FROM node:18.16
WORKDIR /usr/src/TaxEve/api
COPY package.json .
RUN npm install
COPY . . 
EXPOSE 8000
CMD ["npm", "start"]
